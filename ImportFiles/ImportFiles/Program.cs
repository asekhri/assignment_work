﻿using Newtonsoft.Json;
using System;
using System.IO;
using YamlDotNet.Serialization;

namespace ImportFiles
{
    class Program
    {

        static void Main(string[] args)
        {
            using (var reader = new StreamReader(@"..\..\ProductData\capterra.yaml"))
            {
                // Load the stream

                var deserializer = new Deserializer();
                var yamlObject = deserializer.Deserialize(reader);

                //Convert the yaml code to json
                var serializer = new JsonSerializer();
                serializer.Serialize(Console.Out, yamlObject);
            }

            Console.ReadLine();

        }
    }
}
